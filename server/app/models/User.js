const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const emailMatch = [/^\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i, "Invalid email id"];
const UserSchema = new Schema({
  username: String,
  age: Number,
  email: { type: String, lowercase: true, trim: true, unique: true, match: emailMatch, required: true },
});

mongoose.model("User", UserSchema);