const express = require("express");

const router = express.Router();

// get user by userid
router.get("/:userId", (req, res) => {
  res.send("Get User " + req.params.userId);
});

// get users
router.get("/", (req, res) => {
  res.send("Get Users");
});


module.exports = router;