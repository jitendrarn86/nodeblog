const http = require("http");
const fs = require("fs");

const express = require("express");
const mongoose = require("mongoose");

const app = express();

const env = app.settings.env;
const config = require("./config/config")[env];

mongoose.Promise = global.Promise;
mongoose.connect(config.db.connectionString, {
  keepAlive: true,
  reconnectTries: Number.MAX_VALUE
});

// Bootstrap models
var modelsPath = __dirname + "/app/models";
fs.readdirSync(modelsPath).forEach(function (file) {
  if (~file.indexOf(".js")) require(modelsPath + "/" + file);
});

require("./config/express")(app, config);
require('./config/routes')(app);

http.createServer(app).listen(config.httpPort, () => {
  /* eslint-disable no-console */
  console.log(new Date(), ": httpServer listening on 3000");
  /* eslint-enable no-console */
});