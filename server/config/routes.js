const UserController = require("../app/controller/UserController");

module.exports = (app) => {

  app.use("/users", UserController);
  // app.use("/posts", PostController);

  app.get("/", (req, res) => {
    res.send("At root");
  });

  return app;
}