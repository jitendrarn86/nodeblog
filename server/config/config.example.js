module.exports = {
  "development": {
    db: {
      connectionString: "mongodb://localhost:27017/nodeblog"
    },
    secret: "hgjgghgfhgjgfrtyrwererttyuiyyklnbgggfddsaaddv",
    morganLogsType: "tiny",
    uploadsFolder: __dirname + "/../public/uploads",
    httpPort: 3000,
    apiURL: "http://localhost:3000/api",
    allowedOrigins: ["http://localhost:3001"]
  },

  // production config and keys
  "production": {
    
  },
};
